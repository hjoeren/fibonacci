package io.gitlab.hjoeren.fibonacci;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTimeoutPreemptively;
import java.math.BigDecimal;
import java.time.Duration;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.CsvFileSource;

public class FibonacciTest {

  @DisplayName("iterative")
  @ParameterizedTest(name = "{displayName} n = {0}")
  @CsvFileSource(resources = "/fibonacci.csv", numLinesToSkip = 1)
  public void testIterative(int n, BigDecimal fn) {
    assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {
      Fibonacci fibonacci = new IterativeFibonacci();
      BigDecimal result = fibonacci.f(n);
      assertEquals(fn, result);
    });
  }

  @DisplayName("recursive")
  @ParameterizedTest(name = "{displayName} n = {0}")
  @CsvFileSource(resources = "/fibonacci.csv", numLinesToSkip = 1)
  public void testRecursive(int n, BigDecimal fn) {
    assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {
      Fibonacci fibonacci = new RecursiveFibonacci();
      BigDecimal result = fibonacci.f(n);
      assertEquals(fn, result);
    });
  }
  
  @DisplayName("recursiveCached")
  @ParameterizedTest(name = "{displayName} n = {0}")
  @CsvFileSource(resources = "/fibonacci.csv", numLinesToSkip = 1)
  public void testRecursiveCached(int n, BigDecimal fn) {
    assertTimeoutPreemptively(Duration.ofSeconds(5), () -> {
      Fibonacci fibonacci = new RecursiveCachedFibonacci();
      BigDecimal result = fibonacci.f(n);
      assertEquals(fn, result);
    });
  }

}
