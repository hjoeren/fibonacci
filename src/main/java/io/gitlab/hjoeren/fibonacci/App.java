package io.gitlab.hjoeren.fibonacci;

import java.io.IOException;
import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.List;
import org.knowm.xchart.VectorGraphicsEncoder;
import org.knowm.xchart.VectorGraphicsEncoder.VectorGraphicsFormat;
import org.knowm.xchart.XYChart;
import org.knowm.xchart.XYChartBuilder;
import org.knowm.xchart.style.Styler.ChartTheme;

public class App {

  private static int FROM = 0;
  private static int TO = 40;

  public static void main(String[] args) throws IOException, InterruptedException {
    int from;
    int to;

    if (args == null || args.length == 0) {
      from = FROM;
      to = TO;
    } else if (args.length == 2) {
      from = Integer.parseInt(args[0]);
      to = Integer.parseInt(args[1]);
      if (from >= to) {
        System.err.println("Usage: java -jar fibonacci.jar [<from> <to>]");
        System.err.println("<from> must be greater than <to>");
        return;
      }
    } else {
      System.err.println("Usage: java -jar fibonacci [<from> <to>]");
      return;
    }

    Fibonacci iterativeFibonacci = new IterativeFibonacci();
    Fibonacci recursiveFibonacci = new RecursiveFibonacci();
    Fibonacci recursiveCachedFibonacci = new RecursiveCachedFibonacci();

    List<Integer> nData = new ArrayList<>();
    List<Long> iterativeDurationData = new ArrayList<>();
    List<Long> recursiveDurationData = new ArrayList<>();
    List<Long> recursiveCachedDurationData = new ArrayList<>();

    for (int n = from; n < to; n++) {
      nData.add(n);

      Instant start = Instant.now();
      iterativeFibonacci.f(n);
      Duration duration = Duration.between(start, Instant.now());
      iterativeDurationData.add(duration.toNanos());

      start = Instant.now();
      recursiveFibonacci.f(n);
      duration = Duration.between(start, Instant.now());
      recursiveDurationData.add(duration.toNanos());

      start = Instant.now();
      recursiveCachedFibonacci.f(n);
      duration = Duration.between(start, Instant.now());
      recursiveCachedDurationData.add(duration.toNanos());
    }

    XYChartBuilder chartBuilder = new XYChartBuilder();
    chartBuilder.title("Fibonacci");
    chartBuilder.xAxisTitle("n");
    chartBuilder.yAxisTitle("duration [ns]");
    chartBuilder.theme(ChartTheme.Matlab);

    XYChart chart = chartBuilder.build();
    chart.addSeries("iterative", iterativeDurationData);
    chart.addSeries("recursive", recursiveDurationData);
    chart.addSeries("recursive cached", recursiveCachedDurationData);

    VectorGraphicsEncoder.saveVectorGraphic(chart, "./fibonacci", VectorGraphicsFormat.SVG);
  }

}
