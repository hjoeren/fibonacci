package io.gitlab.hjoeren.fibonacci;

import java.math.BigDecimal;

public class RecursiveFibonacci implements Fibonacci {

  @Override
  public BigDecimal f(int n) {
    if (n == 0) {
      return BigDecimal.ZERO;
    } else if (n == 1) {
      return BigDecimal.ONE;
    } else {
      return f(n - 1).add(f(n - 2));
    }
  }

}
