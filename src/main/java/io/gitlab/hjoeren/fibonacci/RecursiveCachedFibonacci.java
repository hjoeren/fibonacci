package io.gitlab.hjoeren.fibonacci;

import java.math.BigDecimal;

public class RecursiveCachedFibonacci implements Fibonacci {

  @Override
  public BigDecimal f(int n) {
    return f(n, new BigDecimal[n]);
  }

  private BigDecimal f(int n, BigDecimal[] cache) {
    if (n == 0) {
      return BigDecimal.ZERO;
    } else if (n == 1) {
      return BigDecimal.ONE;
    } else {
      BigDecimal f1 = cache[n - 1];
      BigDecimal f2 = cache[n - 2];
      if (f1 == null) {
        f1 = f(n - 1, cache);
        cache[n - 1] = f1;
      }
      if (f2 == null) {
        f2 = f(n - 2, cache);
        cache[n - 2] = f2;
      }
      return f1.add(f2);
    }
  }

}
