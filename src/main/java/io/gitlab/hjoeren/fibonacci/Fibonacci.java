package io.gitlab.hjoeren.fibonacci;

import java.math.BigDecimal;

public interface Fibonacci {

  BigDecimal f(int n);

}
