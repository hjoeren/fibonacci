package io.gitlab.hjoeren.fibonacci;

import java.math.BigDecimal;

public class IterativeFibonacci implements Fibonacci {

  @Override
  public BigDecimal f(int n) {
    if (n == 0) {
      return BigDecimal.ZERO;
    } else if (n == 1) {
      return BigDecimal.ONE;
    } else {
      BigDecimal fib1 = BigDecimal.ONE;
      BigDecimal fib2 = BigDecimal.ONE;
      for (long i = 2; i < n; i++) {
        BigDecimal temp = fib1.add(fib2);
        fib1 = fib2;
        fib2 = temp;
      }
      return fib2;
    }
  }

}
